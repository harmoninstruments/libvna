// Harmon Instruments VNA
// Copyright 2016-2020 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#pragma once
#include <array>
#include <complex>

class Matrix2x2 {
public:
        std::array<std::complex<double>, 4> m;
        std::complex<double> det() const { return m[0] * m[3] - m[1] * m[2]; }
        std::complex<double> inv_det();
        void transpose() { std::swap(m[0], m[3]); }
        Matrix2x2 swap_cross() {
                std::swap(m[0], m[3]);
                std::swap(m[1], m[2]);
                return *this;
        }
        void invert();
        // Multiply by another 2x2 matrix
        Matrix2x2 operator*=(const Matrix2x2 &rhs);
        // Multiply by a scalar
        Matrix2x2 operator*=(const std::complex<double> &rhs);
        Matrix2x2(const std::complex<double> a, const std::complex<double> b,
                  const std::complex<double> c, const std::complex<double> d) {
                m[0] = a;
                m[1] = b;
                m[2] = c;
                m[3] = d;
        }
        Matrix2x2() {}
        ~Matrix2x2() {}
        void print(const char *header = "");
};

#ifndef SWIG
static inline Matrix2x2 operator*(Matrix2x2 lhs, const Matrix2x2 &rhs) {
        lhs *= rhs;
        return lhs;
}

static inline Matrix2x2 operator*(Matrix2x2 lhs,
                                  const std::complex<double> &rhs) {
        lhs *= rhs;
        return lhs;
}
#endif

class TParam;
class SParam2 : public Matrix2x2 {
public:
        SParam2(){};
        // conversion from T Parameter constructor
        explicit SParam2(const TParam &tp);
        SParam2(const SParam2 &a, const SParam2 &b);
        SParam2(const SParam2 &a, const SParam2 &b, const SParam2 &c);
        SParam2 inverse();
        std::complex<double> s11() const { return m[0]; }
        std::complex<double> s12() const { return m[1]; }
        std::complex<double> s21() const { return m[2]; }
        std::complex<double> s22() const { return m[3]; }
        void line(double freq, double length, double loss, double zl,
                  double loss_var, double z0 = 50.0);
        double kfactor() const;
        std::complex<double> terminate_1(std::complex<double> x);
        std::complex<double> terminate_2(std::complex<double> x);
        std::complex<double> get_sdd();
        std::complex<double> get_scc();
        std::complex<double> get_scd();
        using Matrix2x2::Matrix2x2;
};

class TParam : public Matrix2x2 {
public:
        TParam(){};
        ~TParam(){};
        // conversion from S Parameter constructor
        explicit TParam(const SParam2 &sp);
        // conversion from base
        explicit TParam(const Matrix2x2 &a) : Matrix2x2(a) {}
        using Matrix2x2::Matrix2x2;
        TParam inverse() {
                TParam rv = *this;
                rv.invert();
                return rv;
        }
        TParam swap_cross() {
                Matrix2x2::swap_cross();
                return *this;
        }
        std::complex<double> t11() const { return m[0]; }
        std::complex<double> t12() const { return m[1]; }
        std::complex<double> t21() const { return m[2]; }
        std::complex<double> t22() const { return m[3]; }
        std::complex<double> inv_s21() const { return m[3]; }
};

SParam2 wgsection(double freq, double a, double l);
SParam2 line(double freq, double length, double loss, double zl,
             double loss_var, double z0 = 50.0);

#ifndef SWIG
static inline TParam operator*(TParam lhs, const TParam &rhs) {
        lhs *= (Matrix2x2)rhs;
        return lhs;
}

static inline TParam operator*(TParam lhs, const std::complex<double> &rhs) {
        lhs *= rhs;
        return lhs;
}
#endif
