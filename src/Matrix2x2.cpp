// Harmon Instruments VNA
// Copyright 2016-2020 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#include <stdexcept>
#include "Matrix2x2.hpp"
#include "format.hpp"
#include "common.hpp"

std::complex<double> Matrix2x2::inv_det() {
        auto x = det();
        if (x == 0.0)
                throw std::runtime_error("Matrix2x2::inv_det not invertable");
        return 1.0 / x;
}

void Matrix2x2::invert() {
        auto idet = inv_det();
        std::swap(m[0], m[3]);
        m[0] *= idet;
        m[3] *= idet;
        m[1] *= -idet;
        m[2] *= -idet;
}

// Multiply by another 2x2 matrix
Matrix2x2 Matrix2x2::operator*=(const Matrix2x2 &rhs) {
        Matrix2x2 rv;
        rv.m[0] = m[0] * rhs.m[0] + m[1] * rhs.m[2];
        rv.m[1] = m[0] * rhs.m[1] + m[1] * rhs.m[3];
        rv.m[2] = m[2] * rhs.m[0] + m[3] * rhs.m[2];
        rv.m[3] = m[2] * rhs.m[1] + m[3] * rhs.m[3];
        m = rv.m;
        return *this;
}

// Multiply by a scalar
Matrix2x2 Matrix2x2::operator*=(const std::complex<double> &rhs) {
        for (auto &x : m)
                x *= rhs;
        return *this;
}

void Matrix2x2::print(const char *header) {
        fmt::print(header);
        for (size_t i = 0; i < 4; i++) {
                fmt::print("({} {}j)\n", m[i].real(), m[i].imag());
        }
}

// conversion from S Parameter constructor
TParam::TParam(const SParam2 &sp) {
        if (sp.s21() == 0.0)
                throw std::runtime_error(
                    "TParam construction from SParam fail");
        auto inv21 = 1.0 / sp.s21();
        m[0] = -sp.det() * inv21;
        m[1] = sp.s11() * inv21;
        m[2] = -sp.s22() * inv21;
        m[3] = inv21;
}

SParam2::SParam2(const TParam &tp) {
        if (tp.m[3] == 0.0)
                throw std::runtime_error(
                    "SParam construction from TParam fail");
        auto inv3 = 1.0 / tp.m[3];
        m[0] = tp.m[1] * inv3;
        m[1] = tp.det() * inv3;
        m[2] = inv3;
        m[3] = -tp.m[2] * inv3;
}

SParam2 SParam2::inverse() {
        auto idet = 1.0 / det();
        auto _s11 = s11() * idet;
        auto _s12 = s21() * -idet;
        auto _s21 = s12() * -idet;
        auto _s22 = s22() * idet;
        return SParam2{_s11, _s12, _s21, _s22};
}

// Concatenate 2 SParam2s
// Ref: Dunsmore, Handbook of Microwave Component Measurements 2.4.3
// Note typo in book: S21/S12 swapped
// [a]--[b]
SParam2::SParam2(const SParam2 &a, const SParam2 &b) {
        auto denom = (1.0 - (a.s22() * b.s11()));
        if (denom == 0.0)
                throw std::runtime_error("div by 0 in cat_2_2");
        auto inv_denom = 1.0 / denom;
        m[0] = a.s11() + (b.s11() * a.s21() * a.s12()) * inv_denom;
        m[1] = a.s12() * b.s12() * inv_denom;
        m[2] = a.s21() * b.s21() * inv_denom;
        m[3] = b.s22() + (a.s22() * b.s21() * b.s12()) * inv_denom;
}

// Concatenate 3 SParam2s
SParam2::SParam2(const SParam2 &a, const SParam2 &b, const SParam2 &c)
    : SParam2(SParam2{a, b}, c) {}

static_assert(sizeof(Matrix2x2) == 4 * sizeof(std::complex<double>),
              "Incorrect size of Matrix2x2");
static_assert(sizeof(SParam2) == 4 * sizeof(std::complex<double>),
              "Incorrect size of SParam2");
static_assert(sizeof(TParam) == 4 * sizeof(std::complex<double>),
              "Incorrect size of TParam2");

SParam2 wgsection(double freq, double a, double l) {
        double wl = C0 / freq; // free space
        double kz = (M_PI / (wl * a)) * std::sqrt(4.0 * a * a - wl * wl);
        auto s21 = std::exp(-I * kz * l);
        return SParam2{0.0, s21, s21, 0.0};
}

// Set the S parameters to those of a lossy line 2 port
// Ref: http://qucs.sourceforge.net/tech/node61.html
// Ref: Matthai, Young, Jones Fig 2.05-3 (p. 28)
// length is in meters
// loss is in dB
// loss_var is in units of ohms per second of delay at 1 GHz
// HP loss in ohm/s = loss(dB)*Zo/(4.3429 dB * delay(s))
void SParam2::line(double freq, double length, double loss, double zl,
                   double loss_var, double z0) {
        // avoid division by zero in the case of zero length or frequency
        if ((freq < 1e-9) || (std::abs(length) < 1e-9)) {
                m[0] = m[3] = 0.0;
                m[1] = m[2] = 1.0;
                return;
        }
        // variable loss in dB/meter
        double vloss = 4.3429 * sqrt(freq * 1e-9) * loss_var / (z0 * C0);
        double alpha = (loss / length + vloss) * db_to_np;
        double beta = 2.0 * M_PI * freq / C0;
        auto gamma = alpha + I * beta;
        auto z11 = zl / tanh(gamma * length);
        auto z21 = zl / sinh(gamma * length);
        auto dzi = 1.0 / ((z11 + z0) * (z11 + z0) - z21 * z21);
        auto s11 = dzi * ((z11 - z0) * (z11 + z0) - z21 * z21);
        auto s21 = dzi * (2.0 * z21 * z0);
        m[0] = m[3] = s11;
        m[1] = m[2] = s21;
}

// Maas Nonlinear Microwave Circuits P. 325
double SParam2::kfactor() const {
        return (1.0 - std::norm(s11()) - std::norm(s22()) + std::norm(det())) /
               (2.0 * std::abs(s12() * s21()));
}

std::complex<double> SParam2::terminate_1(std::complex<double> x) {
        auto denom = 1.0 - (s11() * x);
        if (denom == 0.0) {
                denom = 1.0 - ((s11() + 1e-10) * x);
        }
        return s22() + (x * s21() * s12()) / denom;
}

std::complex<double> SParam2::terminate_2(std::complex<double> x) {
        auto denom = 1.0 - (s22() * x);
        if (denom == 0.0) {
                denom = 1.0 - ((s22() + 1e-10) * x);
        }
        return s11() + (x * s21() * s12()) / denom;
}

// get diff 1 port from 2 port params
std::complex<double> SParam2::get_sdd() {
        return 0.5 * ((s11() + s22()) - (s12() + s21()));
}

// get common 1 port from 2 port params
std::complex<double> SParam2::get_scc() {
        return 0.5 * (s11() + s22() + s12() + s21());
}

// get common mode to differential mode coupling 1 port from 2 port params
std::complex<double> SParam2::get_scd() {
        return 0.5 * (s11() - s12() + s21() + s22());
}

SParam2 line(double freq, double length, double loss, double zl,
             double loss_var, double z0) {
        SParam2 rv;
        rv.line(freq, length, loss, zl, loss_var, z0);
        return rv;
}
